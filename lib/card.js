$(document).ready(function () {
    // Define cards
    var cards = [
        new Tindercardsjs.card(0, 'london big ben', 'description of london big ben', 'gfx/pics/01.jpg'),
        new Tindercardsjs.card(1, 'shanghai the bend', 'description of shanghai the bend', 'gfx/pics/02.jpg'),
        new Tindercardsjs.card(2, 'new york times square', 'description of new york times square', 'gfx/pics/03.jpg'),
        new Tindercardsjs.card(3, 'paris eiffle tower', 'description of eiffle tower', 'gfx/pics/04.jpg')
    ];
    // random shuffle the order of cards
    cards = shuffle(cards);
    // Render cards
    Tindercardsjs.render(cards, $('#main'), function (event) {
        userChoice = {
            'user': 'this user',
            'destination': event.cardid,
            'direction': event.direction
        };
        console.log(userChoice);
    });
});
